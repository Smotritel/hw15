#include <iostream>

void Numbers(int N, int X)
{
    for (int A = X; A <= N; A += 2)
    std::cout << A << " ";
    std::cout << "\n";
}

int main()
{
    int N;
    std::cout << "Enter the number: ";
    std::cin >> N;
    std::cout << "\n";

    std::cout << "Even numbers:\n";
    Numbers(N, 0);
    std::cout << "\n";

    std::cout << "Odd numbers:\n";
    Numbers(N, 1);
    std::cout << "\n";

    return 0;
}
